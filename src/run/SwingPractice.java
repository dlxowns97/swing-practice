package run;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SwingPractice {

	public static void main(String[] args) {

		JFrame mf = new JFrame("더조은 자판기");
		
		mf.setSize(500, 300);
		
		JPanel topPanel = new JPanel();
		JLabel title = new JLabel("더조은 자판기");
		topPanel.setBackground(Color.WHITE);
		topPanel.add(title);
		
		mf.add(topPanel, BorderLayout.NORTH);
		
		JPanel Buttons = new JPanel();
		Buttons.setBackground(new Color(156, 230, 220));
		Buttons.setLayout(new GridLayout(2, 4));
		
		mf.add(Buttons, BorderLayout.CENTER);
		
		JPanel resultPanel = new JPanel();
		JLabel text = new JLabel("아직 음료가 선택되지 않았습니다.");
		resultPanel.setBackground(Color.WHITE);
		resultPanel.add(text);
		
		mf.add(resultPanel, BorderLayout.SOUTH);
//		JButton CiderBtn = new JButton("사이다");
//		JButton FantaBtn = new JButton("환타");
//		JButton CokeBtn = new JButton("콜라");
//		JButton HSBtn = new JButton("핫식스");
//		JButton BakasBtn = new JButton("바카스");
//		JButton MilkBtn = new JButton("우유");
//		JButton LemTeaBtn = new JButton("레몬녹차");
//		JButton OrgJuiceBtn = new JButton("오렌지주스");
		
//		Buttons.add(CiderBtn);
//		Buttons.add(FantaBtn);
//		Buttons.add(CokeBtn);
//		Buttons.add(HSBtn);
//		Buttons.add(BakasBtn);
//		Buttons.add(MilkBtn);
//		Buttons.add(LemTeaBtn);
//		Buttons.add(OrgJuiceBtn);
		
		
		
		List<String> drinks = new ArrayList<>();
		drinks.add("사이다");
		drinks.add("환타");
		drinks.add("콜라");
		drinks.add("핫식스");
		drinks.add("바카스");
		drinks.add("우유");
		drinks.add("레몬녹차");
		drinks.add("오렌지주스");
		
		JButton[] Btns = new JButton[drinks.size()];
		
		for(int i = 0; i < Btns.length; i++) {
			Btns[i] = new JButton(drinks.get(i));
			
			Buttons.add(Btns[i]);
		}
		
		for(int i = 0; i < Btns.length; i++) {
			Btns[i].addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					String drink = ((JButton) e.getSource()).getText();
					int price = 0;
					
					switch(drink) {
						case "사이다" :
							price = 1000;
							break;
						case "환타" :
							price = 1000;
							break;
						case "콜라" :
							price = 2000;
							break;
						case "핫식스" :
							price = 1500;
							break;
						case "바카스" :
							price = 800;
							break;
						case "우유" :
							price = 750;
							break;
						case "레몬녹차" :
							price = 1800;
							break;
						case "오렌지주스" :
							price = 2000;
							break;
					}
					
					text.setText(drink + "가 선택되었습니다. " + price + "원을 투입해주세요.");
					
				}
			});
		}
		

		
//		CiderBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//
//				text.setText("사이다가 선택되었습니다. 1000원을 투입해주세요.");
//				
//			}
//		});
//		
//		FantaBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				text.setText("환타가 선택되었습니다. 1000원을 투입해주세요.");
//				
//			}
//		});
//		
//		CokeBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				text.setText("콜라가 선택되었습니다. 2000원을 투입해주세요.");
//				
//			}
//		});
//		
//		HSBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				text.setText("핫식스가 선택되었습니다. 1500원을 투입해주세요.");
//				
//			}
//		});
//		
//		BakasBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				text.setText("바카스가 선택되었습니다. 800원을 투입해주세요.");
//				
//			}
//		});
//		
//		MilkBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				text.setText("우유가 선택되었습니다. 750원을 투입해주세요.");
//				
//			}
//		});
//		
//		LemTeaBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				text.setText("레몬녹차가 선택되었습니다. 1800원을 투입해주세요.");
//				
//			}
//		});
//		
//		OrgJuiceBtn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				text.setText("오렌지주스가 선택되었습니다. 2000원을 투입해주세요.");
//				
//			}
//		});
		
		mf.setVisible(true);
		mf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}
